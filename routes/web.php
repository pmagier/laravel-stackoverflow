<?php

use App\Models\Answer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\FavoritesController;
use App\Http\Controllers\VoteAnswerController;
use App\Http\Controllers\GithubLoginController;
use App\Http\Controllers\AcceptAnswerController;
use App\Http\Controllers\VoteQuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes();

//github routes
Route::get('/login/github', [GithubLoginController::class, 'redirect'])->name('login.github-redirect');
Route::get('/login/github/callback', [GithubLoginController::class, 'callback']);

Route::view('/{any}', 'spa')->where('any', '.*');


Route::get('/', [QuestionController::class, 'index']);


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('questions', QuestionController::class)->except('show');
// Route::post('/questions/{question}/answers', [AnswerController::class, 'store'] ) -> name('answers.store');
Route::resource('questions.answers', AnswerController::class)->only(['store', 'edit', 'update', 'destroy', 'index']);
Route::get('/questions/{slug}', [QuestionController::class, 'show'])->name('questions.show');
Route::post('/answers/{answer}/accept', AcceptAnswerController::class)->name('answers.accept');

Route::post('/questions/{question}/favorites', [FavoritesController::class, 'store'])->name('questions.favorite');
Route::delete('/questions/{question}/favorites', [FavoritesController::class, 'destroy'])->name('questions.unfavorite');

Route::post('/questions/{question}/vote', VoteQuestionController::class);

Route::post('/answers/{answer}/vote', VoteAnswerController::class);

