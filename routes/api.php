<?php

// use App\Http\Controllers\Auth\LoginController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\Auth\LoginController;

use App\Http\Controllers\Api\AnswerController;
use App\Http\Controllers\Api\FavoritesController;
use App\Http\Controllers\Api\QuestionsController;
use App\Http\Controllers\Api\VoteAnswerController;
use App\Http\Controllers\Api\AcceptAnswerController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\MyPostsController;
use App\Http\Controllers\Api\VoteQuestionController;
use App\Http\Controllers\Api\QuestionDetailController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::post('/token', [LoginController::class, "getToken"]);

Route::post('/login', [LoginController::class, "store"]);
Route::post('/register', RegisterController::class);

Route::get('/questions', [QuestionsController::class, "index"]);
Route::get('/questions/{question}/answers', [AnswerController::class, "index"]);

Route::get('/questions/{question}-{slug}', QuestionDetailController::class);

Route::middleware(['auth:api'])->group(function () {
    Route::apiResource('/questions', QuestionsController::class)->except('index');
    Route::apiResource('/questions.answers', AnswerController::class)->except('index');
    Route::post('/questions/{question}/vote', VoteQuestionController::class);
    Route::post('/answers/{answer}/vote', VoteAnswerController::class);

    Route::post('/answers/{answer}/accept', AcceptAnswerController::class)->name('answers.accept');

    Route::post('/questions/{question}/favorites', [FavoritesController::class, 'store'])->name('questions.favorite');
    Route::delete('/questions/{question}/favorites', [FavoritesController::class, 'destroy'])->name('questions.unfavorite');

    Route::get('/my-posts', MyPostsController::class);

    Route::delete('/logout', [LoginController::class, "destroy"]);
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
