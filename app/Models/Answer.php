<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Mews\Purifier\Facades\Purifier;

class Answer extends Model
{
    use HasFactory;

    protected $fillable = ['body', 'user_id'];

    protected $appends = ['created_date', 'body_html', 'is_best'];

    public function question(){
        return $this->belongsTo(Question::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function votes(){
        return $this->morphToMany(User::class, 'votable');
    }

    public function getBodyHtmlAttribute()
    {
        $markdown = new CommonMarkConverter(['allow_unsafe_links' => false]);

        return Purifier::clean($markdown->convert($this->body));
    }

    public function getCreatedDateAttribute(){
        return $this->created_at->diffForHumans();
    }

    public function getStatusAttribute(){
        return $this->isBest() ? 'vote-accepted' : '';
    }

    public function getIsBestAttribute(){
        return $this->isBest();
    }

    public function isBest(){
        return $this->id === $this->question->best_answer_id;
    }

    public static function boot()
    {
        parent::boot();
    
        static::created(function (Answer $answer) {
            $answer->question->increment('answers_count');
        });

        static::deleted(function(Answer $answer){
            $answer->question->decrement('answers_count');

        });
    }


    public function downVotes(){
        return $this->votes()->wherePivot('vote', -1);
    }

    public function upVotes(){
        return $this->votes()->wherePivot('vote', 1);
    }
    
}
