<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class AnswerController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index(Question $question){
        return $question->answers()->with('user')->simplePaginate(3);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Question $question, Request $request)
    {   

        $request -> validate([
            'body' => 'required'
        ]);

        $answer = $question->answers()->create([
            'body'    => $request->body,
            'user_id' => \Auth::id()
        ]);

        if ($request -> expectsJson()){
            return response() -> json([
                'message' => "You answer has been submitted succesfully",
                'answer' => $answer->load('user')
            ]);
        }

        return back()->with('success', "You answer has been submitted succesfully");
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Question $question, Answer $answer)
    {
        $this->authorize('update', $answer);

        return view('answers.edit', compact('question', 'answer'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Question $question, Answer $answer)
    {
        $this->authorize('update', $answer);

        $answer->update($request->validate([
            'body' => 'required'
        ]));

        if($request -> expectsJson()){
            return response()->json([
                'message' => 'Your answer has been updated',
                'body_html' => $answer->body_html
            ]);
        }

        return redirect()->route('questions.show', $question->slug)->with('success', "Your answer has been edited");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question, Answer $answer)
    {
        $this->authorize('delete', $answer);

        $answer->delete();

        if (request()->expectsJson())
        {
            return response()->json([
                'message' => "Your answer has been removed"
            ]);
        }

        return back()->with('success', "You answer has been deleted succesfully");


    }
}
