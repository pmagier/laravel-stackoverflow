<?php

namespace App\Http\Controllers\Api;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $questions = Question::with('user')->latest()->paginate(5);
        sleep(2);
        return QuestionResource::collection($questions);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request -> validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        $question = $request->user()->questions()->create($request->only('title', 'body')); //instead of only() it could be used all()

        return response()->json([
            'message' => 'Your question has been submitted',
            'question' => new QuestionResource($question)
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Question $question)
    {
        return response()->json([
            'title'     => $question->title,
            'body'      => $question->body,
            'body_html' => $question->body_html
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Question $question)
    {
        $this->authorize("update", $question);

        $question->update($request->only('title', 'body'));

        return response()->json([
            'message'   => "Your question has been updated",
            'body_html' => $question->body_html
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question)
    {
        $this->authorize("delete", $question);

        $question->delete();

        return response()->json([
            'message' => "Your question has been deleted",
        ]);
    }
}
