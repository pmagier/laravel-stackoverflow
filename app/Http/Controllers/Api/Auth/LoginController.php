<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function store(Request $request)
    {

        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        $req = Request::create('/oauth/token', 'POST', [
            'grant_type'    => "password",
            'client_id'     => 16,
            'client_secret' => 'xnSYotI6WxxzVCHJnC2CtOcWu166ZAGCNyoKV5JM',
            'username'      => request('username'),
            'password'      => request('password'),
            'scope'         => ''
        ]);

        $res = app()->handle($req);
        $responseBody = json_decode($res->getContent()); // convert to json object
        return response()->json(['success' => $responseBody], $res->getStatusCode());
    }

    public function destroy(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->noContent();
    }
}
