<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/my-posts';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getToken(Request $request)
    {

        $req = Request::create('/oauth/token', 'POST', [
            'grant_type'    => "password",
            'client_id'     => 16,
            'client_secret' => 'xnSYotI6WxxzVCHJnC2CtOcWu166ZAGCNyoKV5JM',
            'username'      => request('email'),
            'password'      => request('password'),
            'scope'         => ''
        ]);

        $res = app()->handle($req);
        $responseBody = json_decode($res->getContent()); // convert to json object
        return response()->json(['success' => $responseBody], $res->getStatusCode());


    }

}
