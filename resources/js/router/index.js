import { createRouter, createWebHistory } from "vue-router";
//Importing page components
import QuestionsPage from "../pages/QuestionsPage.vue";
import QuestionPage from "../pages/QuestionPage.vue";
import MyPostsPage from "../pages/MyPostsPage.vue";
import CreateQuestionPage from "../pages/CreateQuestionPage.vue";
import EditQuestionPage from "../pages/EditQuestionPage.vue";
import LoginPage from "../pages/auth/LoginPage.vue";
import RegisterPage from "../pages/auth/RegisterPage.vue";
import NotFoundPage from "../pages/NotFoundPage.vue";

//Defining routes

const routes = [
    { path: "/", component: QuestionsPage, name: "home" },
    { path: "/questions", component: QuestionsPage, name: "questions" },
    { path: "/questions/:slug", component: QuestionPage, name: "question.show", props: true},
    { path: "/my-posts", component: MyPostsPage, name: "my-posts", meta:{ requiresAuth: true }},
    { path: "/questions/create", component: CreateQuestionPage, name: "question.create" },
    { path: "/questions/:id/edit", component: EditQuestionPage, name: "question.edit" },

    //auth
    { path: "/login", component: LoginPage, name: "login" }, 
    { path: "/register", component: RegisterPage, name: "register" }, 

    //end of routes
    { path: '/:pathMatch(.*)*', component: NotFoundPage}
];

//creating router instance
const router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: "active",
});

router.beforeEach((to, from, next)=>{
    if(to.matched.some(r => r.meta.requiresAuth) && !window.Auth.signedIn){
        window.location = window.Urls.login
        return
    }

    next()
})

export default router;
