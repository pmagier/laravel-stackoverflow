import { config, library, dom } from '@fortawesome/fontawesome-svg-core';
config.autoReplaceSvg = 'nest';

import {faCaretUp,
        faCaretDown,
        faStar,
        faCheck,
        faSpinner,} from '@fortawesome/free-solid-svg-icons';

import { faGithub } from '@fortawesome/free-brands-svg-icons';

library.add(faCaretUp,
            faCaretDown,
            faStar,
            faCheck,
            faSpinner, faGithub);

dom.watch();