export default {
    methods: {
        destroy() {
            if (confirm("Are you sure?")) {
                this.delete();
            }
        },

        delete() {},
    },
};
