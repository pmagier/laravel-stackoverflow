import UserInfo from "../components/UserInfo.vue";
import Vote from "../components/Vote.vue";
import MEditor from '../components/MEditor.vue';
import highlight from "./highlight";
import destroy from "./destroy";

export default {
    mixins: [highlight, destroy],
    data(){
        return{
            editing: false,
        }
    },

    components: { Vote, UserInfo , MEditor},


    methods:{
        edit() {
            this.setEditCache();
            this.editing = true;
        },
        cancel(){
            this.restoreFromCache();
            this.editing = false;
            this.highlight()
        },

        setEditCache(){},
        restoreFromCache(){},

        update() {
            axios.put(this.endpoint, this.payload())
            .catch(({response}) =>{
                alert(response.data.message)
            })
            .then(({data})=>{
                this.bodyHtml = data.body_html;
                alert(data.message)
                this.editing = false
            })
            .then(()=> this.highlight())
        },

        payload(){},
    }
}