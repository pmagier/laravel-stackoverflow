import policies from './policies';

export default{
    install (Vue, options){
        Vue.config.globalProperties.authorize = function (policy, model){
            if(!window.Auth.signedIn) return false;
        
            if(typeof policy === 'string' && typeof model === 'object'){
                const user = window.Auth.user;
        
                return policies[policy](user, model);
            }
        },

        Vue.config.globalProperties.signedIn = window.Auth.signedIn
    }
}

