<span class="text-muted"> {{ $label . " " . $model->created_date }}</span>
<div class="media mt-2">
    <a href="{{ $model->user->url ?? 'null'}}" class="pr-2">
        <img src="{{ $model->user->avatar ?? 'null' }}">
    </a>
    <div class="media-body mt-1">
        <a href="{{ $model->user->url ?? 'null' }}">{{ $model->user->name ?? 'null' }}</a>
    </div>
</div>